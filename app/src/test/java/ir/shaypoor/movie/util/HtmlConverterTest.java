package ir.shaypoor.movie.util;

import android.text.Spanned;
import android.text.SpannedString;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import ir.shaypoor.movie.BuildConfig;
import ir.shaypoor.movie.base.MasterApplication;
import ir.shaypoor.movie.utils.HtmlConverter;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21
//        ,manifest = "/src/main/AndroidManifest.xml"
        , application = MasterApplication.class)

public class HtmlConverterTest extends TestCase {

    @Test
    public void FromHtml_htmlEqualText_shouldPass() {
        Spanned test = HtmlConverter.fromHtml("<a>hello</a>");
        assertEquals(test.toString(),new SpannedString("hello").toString());
    }

    @Test
    public void FromHtml_simpleTextAsInput_shouldPass() {
        Spanned test = HtmlConverter.fromHtml("hello");
        assertEquals(test.toString(),new SpannedString("hello").toString());
    }

    @Test(expected = NullPointerException.class)
    public void FromHtml_nullInput_shouldFail() {
        HtmlConverter.fromHtml(null);
    }
}