package ir.shaypoor.movie.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ashkan on 12/3/17.
 */

public class Ratings implements Parcelable {
    double average;

    public double getAverage() {
        return average;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.average);
    }

    public Ratings() {
    }

    protected Ratings(Parcel in) {
        this.average = in.readDouble();
    }

    public static final Parcelable.Creator<Ratings> CREATOR = new Parcelable.Creator<Ratings>() {
        @Override
        public Ratings createFromParcel(Parcel source) {
            return new Ratings(source);
        }

        @Override
        public Ratings[] newArray(int size) {
            return new Ratings[size];
        }
    };
}
