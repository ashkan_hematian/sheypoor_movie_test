package ir.shaypoor.movie.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ashkan on 12/3/17.
 */

public class Images implements Parcelable {

    String medium;
    String original;


    public String getMedium() {
        return medium;
    }
    public String getOriginal() {
        return original;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.medium);
        dest.writeString(this.original);
    }

    public Images() {
    }

    protected Images(Parcel in) {
        this.medium = in.readString();
        this.original = in.readString();
    }

    public static final Parcelable.Creator<Images> CREATOR = new Parcelable.Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel source) {
            return new Images(source);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };
}
