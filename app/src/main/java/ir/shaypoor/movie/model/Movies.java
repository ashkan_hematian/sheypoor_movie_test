package ir.shaypoor.movie.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ashkan on 12/2/17.
 */

public class Movies implements Parcelable {
    long id;
    String url;
    String name;
    @SerializedName("rating")
    Ratings rates;
    @SerializedName("image")
    Images images;
    @SerializedName("runtime")
    int duration;
    @SerializedName("premiered")
    String publishDate;
    String summary;


    public long getId() {
        return id;
    }
    public String getUrl() {
        return url;
    }
    public String getName() {
        return name;
    }
    public Ratings getRates() {
        return rates;
    }
    public Images getImages() {
        return images;
    }
    public int getDuration() {
        return duration;
    }
    public String getPublishDate() {
        return publishDate;
    }
    public String getYearOfPublishDate() {
        String[] dateSplit = publishDate.split("-");
        if (dateSplit.length > 0)
            return dateSplit[0];
        else
            return "";
    }
    public String getSummary() {
        return summary;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.url);
        dest.writeString(this.name);
        dest.writeParcelable(this.rates, flags);
        dest.writeParcelable(this.images, flags);
        dest.writeInt(this.duration);
        dest.writeString(this.publishDate);
        dest.writeString(this.summary);
    }

    public Movies() {
    }

    protected Movies(Parcel in) {
        this.id = in.readLong();
        this.url = in.readString();
        this.name = in.readString();
        this.rates = in.readParcelable(Ratings.class.getClassLoader());
        this.images = in.readParcelable(Images.class.getClassLoader());
        this.duration = in.readInt();
        this.publishDate = in.readString();
        this.summary = in.readString();
    }

    public static final Creator<Movies> CREATOR = new Creator<Movies>() {
        @Override
        public Movies createFromParcel(Parcel source) {
            return new Movies(source);
        }

        @Override
        public Movies[] newArray(int size) {
            return new Movies[size];
        }
    };
}
