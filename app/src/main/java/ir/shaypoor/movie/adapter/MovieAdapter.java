package ir.shaypoor.movie.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import ir.shaypoor.movie.R;
import ir.shaypoor.movie.model.Movies;

public class MovieAdapter <T extends Movies> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<T> data;
    private View.OnClickListener mCallListener;


    public MovieAdapter(List<T> data) {
        this.data = data;
    }

    public void setmCallListener(View.OnClickListener mCallListener) {
        this.mCallListener = mCallListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_mvoie, viewGroup, false);

        RecyclerView.ViewHolder vh;
        vh = new DefaultViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder mHolder, final int position) {
        DefaultViewHolder holder = (DefaultViewHolder) mHolder;

        T node = data.get(position);
        ImageLoader.getInstance().displayImage(node.getImages().getOriginal(), holder.poster_iv);

        holder.baseView.setTag(node);
        holder.baseView.setOnClickListener(mCallListener);
    }

    @Override
    public int getItemCount() {
        if(data==null)
            return 0;
        else
            return data.size();
    }

    public void setData(List<T> mList) {
        this.data = mList;
    }

    //region viewHolder
    public class DefaultViewHolder extends RecyclerView.ViewHolder {

        protected View baseView;
        protected ImageView poster_iv;


        public DefaultViewHolder(View view) {
            super(view);
            baseView = view;
            poster_iv = (ImageView) view.findViewById(R.id.poster_imageView);
        }

    }

    //endregion

}
