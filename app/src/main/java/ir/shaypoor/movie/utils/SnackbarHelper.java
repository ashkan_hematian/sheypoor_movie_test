package ir.shaypoor.movie.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import ir.shaypoor.movie.R;
import ir.shaypoor.movie.base.MasterApplication;


public class SnackbarHelper {
    private static SnackbarHelper sInstance;
    private Context context = MasterApplication.sInstance;
    private SnackbarHelper() {
    }

    public static SnackbarHelper getInstance() {
        if (sInstance == null) {
            sInstance = new SnackbarHelper();
        }
        return sInstance;
    }

    public void showNormalSnack(View baseView,
                                String message,
                                int status) {
        Snackbar snackbar = Snackbar.make(baseView, message, 3000);
        // get snackbar view
        View snackbarView = snackbar.getView();

        // change snackbar text color
        int snackbarTextId = android.support.design.R.id.snackbar_text;
        TextView textView = (TextView) snackbarView.findViewById(snackbarTextId);

        if (status == -1) {
            snackbarView.setBackgroundColor(ContextCompat.getColor(context, R.color.md_red_300));
        } else if (status == 1) {
            snackbarView.setBackgroundColor(ContextCompat.getColor(context, R.color.md_green_200));
        } else {
            textView.setTextColor(ContextCompat.getColor(context, R.color.md_blue_grey_800));
            snackbarView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }
        snackbar.show();

    }

}
