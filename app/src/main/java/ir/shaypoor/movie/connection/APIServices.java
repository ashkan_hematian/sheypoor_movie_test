package ir.shaypoor.movie.connection;

import java.util.List;

import ir.shaypoor.movie.model.Movies;
import retrofit2.Call;
import retrofit2.http.GET;

public interface APIServices {


    @GET("/shows")
    Call<List<Movies>> GetShowList();


}