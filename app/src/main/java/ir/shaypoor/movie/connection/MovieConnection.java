package ir.shaypoor.movie.connection;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import ir.shaypoor.movie.R;
import ir.shaypoor.movie.base.ConnectionInterface;
import ir.shaypoor.movie.base.MasterApplication;
import ir.shaypoor.movie.model.Movies;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieConnection {

    Context context = MasterApplication.sInstance;
    private ConnectionInterface mListener;
    private String TAG = "movieConnection";
    private String baseUrl = "https://api.tvmaze.com/";


    public void getMovieList(ConnectionInterface listener) {
        this.mListener = listener;

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIServices service = retrofit.create(APIServices.class);
        Call<List<Movies>> call = service.GetShowList();

        call.enqueue(new Callback<List<Movies>>() {
            @Override
            public void onResponse(Call<List<Movies>> call, Response<List<Movies>> response) {
                Log.i(TAG, response.toString());
                if (response.isSuccessful()) {
                    mListener.successListener(response.body());
                } else {
                    mListener.errorListener(context.getString(R.string.error_server_connection));
                }
            }

            @Override
            public void onFailure(Call<List<Movies>> call, Throwable t) {
                Log.e(TAG, t.getLocalizedMessage());
                mListener.errorListener(context.getString(R.string.error_received_data));
            }
        });
    }

}
