package ir.shaypoor.movie.model;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4.class)
public class RatingTest extends TestCase {

    @Test
    public void Rating_parcelable_shouldPass() {
        Ratings rating = new Ratings();
        rating.average = 1.1;

        Parcel parcel = Parcel.obtain();
        rating.writeToParcel(parcel, rating.describeContents());
        parcel.setDataPosition(0);

        Ratings createdFromParcel = Ratings.CREATOR.createFromParcel(parcel);
        assertEquals(createdFromParcel.getAverage(), 1.1);
    }
}