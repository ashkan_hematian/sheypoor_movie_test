package ir.shaypoor.movie.model;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4.class)
public class ImagesTest extends TestCase {

    @Test
    public void Image_parcelable_shouldPass() {
        Images image = new Images();
        image.medium = "med";
        image.original = "orig";

        Parcel parcel = Parcel.obtain();
        image.writeToParcel(parcel, image.describeContents());
        parcel.setDataPosition(0);

        Images createdFromParcel = Images.CREATOR.createFromParcel(parcel);
        assertEquals(createdFromParcel.getMedium(), "med");
        assertEquals(createdFromParcel.getOriginal(), "orig");
    }
}