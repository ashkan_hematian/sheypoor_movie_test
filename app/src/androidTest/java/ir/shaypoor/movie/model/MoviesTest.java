package ir.shaypoor.movie.model;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4.class)
public class MoviesTest extends TestCase {

    @Test
    public void Movies_parcelable_shouldPass() {
        Movies movie = new Movies();
        movie.duration = 1;
        movie.id = 2;
        movie.url = "url";
        movie.summary = "summary";
        movie.publishDate = "pDate";
        movie.name = "name";

        Parcel parcel = Parcel.obtain();
        movie.writeToParcel(parcel, movie.describeContents());
        parcel.setDataPosition(0);

        Movies createdFromParcel = Movies.CREATOR.createFromParcel(parcel);
        assertEquals(createdFromParcel.getDuration(), 1);
        assertEquals(createdFromParcel.getId(), 2);
        assertEquals(createdFromParcel.getName(), "name");
        assertEquals(createdFromParcel.getPublishDate(), "pDate");
        assertEquals(createdFromParcel.getSummary(), "summary");
        assertEquals(createdFromParcel.getUrl(), "url");
    }
}